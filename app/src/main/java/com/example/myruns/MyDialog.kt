package com.example.myruns;

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context.MODE_PRIVATE
import android.os.Bundle
import android.view.View
import android.widget.EditText
import androidx.fragment.app.DialogFragment

class MyDialog : DialogFragment() {
    private val SETTINGSPREFS = "settingsprefs"
    private val ENTRYPREFS = "entryprefs"
    private val COMMENTS = "comments"

    companion object {
        const val DIALOG_KEY = "dialog"
        const val DEC_DIALOG = 0
        const val NUM_DIALOG = 1
        const val COMMENT_DIALOG = 2
        const val COMMENTSSETTING_DIALOG = 3
        const val TITLE_KEY = "title"
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        lateinit var ret: Dialog
        val bundle = arguments
        val dialogId = bundle!!.getInt(DIALOG_KEY)

        val builder = AlertDialog.Builder(requireActivity())
        val view: View =
            requireActivity().layoutInflater.inflate(R.layout.fragment_manual_dialog, null)
        val dialogEdit: EditText = view.findViewById(R.id.dialog_edit)

        val title = bundle.getString(TITLE_KEY)
        builder.setTitle(title)

        if (dialogId == COMMENT_DIALOG) {
            dialogEdit.inputType = android.text.InputType.TYPE_CLASS_TEXT
            dialogEdit.hint = getString(R.string.comments_hint)
        } else if (dialogId == COMMENTSSETTING_DIALOG) {
            val sharedPreferences =
                requireActivity().getSharedPreferences(SETTINGSPREFS, MODE_PRIVATE)
            dialogEdit.inputType = android.text.InputType.TYPE_CLASS_TEXT
            dialogEdit.setText(sharedPreferences?.getString(COMMENTS, ""))
        } else if (dialogId == NUM_DIALOG) {
            dialogEdit.inputType = android.text.InputType.TYPE_CLASS_NUMBER
        }

        builder.setPositiveButton(
            "Ok"
        ) { dialog, which ->
            if (dialogId == NUM_DIALOG) {
                val sharedPreferences = requireActivity().getSharedPreferences(
                    ENTRYPREFS, MODE_PRIVATE
                )
                val editor = sharedPreferences.edit()
                editor.putInt(title, Integer.parseInt(dialogEdit.text.toString()))
                editor.apply()
            } else if (dialogId == COMMENTSSETTING_DIALOG) {
                val sharedPreferences = requireActivity().getSharedPreferences(
                    SETTINGSPREFS, MODE_PRIVATE
                )
                val editor = sharedPreferences.edit()
                editor.putString(COMMENTS, dialogEdit.text.toString())
                editor.apply()
            } else if (dialogId == DEC_DIALOG) {
                val sharedPreferences = requireActivity().getSharedPreferences(
                    ENTRYPREFS, MODE_PRIVATE
                )
                val editor = sharedPreferences.edit()
                editor.putFloat(title, dialogEdit.text.toString().toFloat())
                editor.apply()
            }
            dialog.dismiss()
        }

        builder.setNegativeButton("Cancel") { dialog, which ->
            dialog.dismiss()
        }

        builder.setView(view)
        ret = builder.create()
        return ret
    }
}