package com.example.myruns

import android.graphics.Bitmap
import android.graphics.Canvas
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.RadioGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.FileProvider
import java.io.File
import java.io.FileOutputStream
import java.io.IOException


class ProfileActivity : AppCompatActivity() {
    private lateinit var saveImgUri: Uri
    private lateinit var imageView: ImageView
    private val savedImgFileName = "saved.jpg"

    private val PREFS = "prefs"
    private val NAME = "name"
    private val NUMBER = "number"
    private val EMAIL = "email"
    private val YEAR = "year"
    private val GENDER = "gender"
    private val MAJOR = "major"

    lateinit var name: EditText
    lateinit var number: EditText
    lateinit var email: EditText
    lateinit var year: EditText
    lateinit var major: EditText
    lateinit var gender: RadioGroup


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)

        val sharedPreferences = getSharedPreferences(PREFS, MODE_PRIVATE)

        name = findViewById(R.id.name_edit)
        number = findViewById(R.id.number_edit)
        email = findViewById(R.id.email_edit)
        year = findViewById(R.id.year_edit)
        major = findViewById(R.id.major_edit)
        gender = findViewById(R.id.gender_group)

        name.setText(sharedPreferences?.getString(NAME, ""))
        email.setText(sharedPreferences?.getString(EMAIL, ""))
        major.setText(sharedPreferences?.getString(MAJOR, ""))

        if (sharedPreferences.contains(NUMBER))
            number.setText(sharedPreferences?.getInt(NUMBER, 0).toString())
        if (sharedPreferences.contains(YEAR))
            year.setText(sharedPreferences?.getInt(YEAR, 0).toString())
        sharedPreferences?.getInt(GENDER, 0)?.let { gender.check(it) }

        val saveImgFile =
            File(getExternalFilesDir(Environment.DIRECTORY_PICTURES), savedImgFileName)
        saveImgUri = FileProvider.getUriForFile(this, "com.example.myruns", saveImgFile)

        imageView = findViewById(R.id.profile_image)
        if (saveImgFile.exists()) {
            imageView.setImageURI(saveImgUri)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.main, menu)
        return true
    }

    private fun getBitmapFromView(view: View): Bitmap {
        val bitmap =
            Bitmap.createBitmap(view.width, view.height, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        view.draw(canvas)
        return bitmap
    }

    fun updateProfile(view: View) {

        try {
            val saveImgFile =
                File(getExternalFilesDir(Environment.DIRECTORY_PICTURES), savedImgFileName)
            val stream = FileOutputStream(saveImgFile)
            val map: Bitmap = getBitmapFromView(imageView)
            map.compress(Bitmap.CompressFormat.PNG, 90, stream)
            stream.flush()
            stream.close()
        } catch (e: IOException) {
            e.printStackTrace()
        }

        val sharedPreferences = getSharedPreferences(PREFS, MODE_PRIVATE)
        val editor = sharedPreferences.edit()

        if (editor != null) {
            editor.putString(NAME, name.text.toString())
            editor.putString(EMAIL, email.text.toString())
            editor.putString(MAJOR, major.text.toString())

            gender.checkedRadioButtonId.let { editor.putInt(GENDER, it) }
            editor.putInt(NUMBER, Integer.parseInt(number.text.toString()))
            editor.putInt(YEAR, Integer.parseInt(year.text.toString()))
            editor.apply()
            Toast.makeText(this, "Data saved.", Toast.LENGTH_SHORT).show()
        }
        finish()
    }

    fun cancelUpdate(view: View) {
        finish()
    }

    fun onChangePhotoClicked(view: View) {
        showImageDialog()
    }

    private fun showImageDialog() {
        val cameraDialog = CameraDialog()
        cameraDialog.show(supportFragmentManager, "image dialog")

    }
}



