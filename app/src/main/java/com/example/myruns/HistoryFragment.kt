package com.example.myruns

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.myruns.adapters.EntryListAdapter
import com.example.myruns.data.AppDatabase
import com.example.myruns.data.Entry
import com.example.myruns.data.EntryDao
import com.example.myruns.data.EntryRepository
import com.example.myruns.viewmodels.EntryViewModel
import com.example.myruns.viewmodels.EntryViewModelFactory


class HistoryFragment : Fragment() {
    private lateinit var arrayList: ArrayList<Entry>
    private lateinit var arrayAdapter: EntryListAdapter
    private lateinit var myListView: ListView

    private lateinit var database: AppDatabase
    private lateinit var databaseDao: EntryDao
    private lateinit var repository: EntryRepository

    private lateinit var viewModelFactory: EntryViewModelFactory
    private lateinit var entryViewModel: EntryViewModel

    companion object {
        const val HISTPREF = "history"
        const val POSPREF = "position"

        const val INTTYPE = "input"
        const val ACTTYPE = "activity"

        const val DATETIME = "datetime"
        const val DURATION = "duration"
        const val DISTANCE = "distance"
        const val CALORIES = "calories"
        const val HEARTRATE = "heartrate"
        const val AVGSPEED = "avgspeed"
        const val CLIMB = "climb"
        const val LOCATIONS = "locations"
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_history, container, false)

        myListView = view.findViewById(R.id.history_list)

        arrayList = ArrayList()
        arrayAdapter = EntryListAdapter(requireActivity(), arrayList)
        myListView.adapter = arrayAdapter

        database = AppDatabase.getInstance(requireActivity())
        databaseDao = database.entryDao
        repository = EntryRepository(databaseDao)

        viewModelFactory = EntryViewModelFactory(repository)

        entryViewModel =
            ViewModelProvider(requireActivity(), viewModelFactory).get(EntryViewModel::class.java)

        return view
    }

    override fun onResume() {
        super.onResume()

        val sharedPreferences =
            requireActivity().getSharedPreferences(HISTPREF, Context.MODE_PRIVATE)
        val delete = sharedPreferences.getBoolean(HISTPREF, false)
        val editor = sharedPreferences.edit()

        if (delete) {
            val position = sharedPreferences.getInt(POSPREF, -1)
            if (position >= 0) {
                entryViewModel.delete(position)
                editor.putBoolean(HISTPREF, false)
                editor.apply()
            }
        }

        entryViewModel.allEntriesLiveData.observe(requireActivity(), Observer {
            arrayAdapter.replace(it)
            arrayAdapter.notifyDataSetChanged()

            myListView.setOnItemClickListener() { parent, itemView, position, id ->
                editor.putInt(POSPREF, position)

                val entryType = it.get(position).entryType
                if (entryType.lowercase() == "manual entry") {
                    val entryIntent = Intent(activity, DisplayEntryActivity::class.java)

                    editor.putString(INTTYPE, it.get(position).entryType)
                    editor.putString(ACTTYPE, it.get(position).activityType)
                    editor.putString(DATETIME, it.get(position).dataTime)
                    editor.putFloat(DURATION, it.get(position).duration)
                    editor.putFloat(DISTANCE, it.get(position).distance)
                    editor.putInt(CALORIES, it.get(position).calories)
                    editor.putInt(HEARTRATE, it.get(position).heartRate)

                    editor.apply()
                    startActivity(entryIntent)
                } else {
                    val mapIntent = Intent(activity, MapsActivity::class.java)

                    mapIntent.putExtra(StartFragment.INTTYPE, it.get(position).entryType)
                    mapIntent.putExtra(StartFragment.ACTTYPE, it.get(position).activityType)

                    mapIntent.putExtra(AVGSPEED, it.get(position).avgSpeed)
                    mapIntent.putExtra(CLIMB, it.get(position).climb)
                    mapIntent.putExtra(CALORIES, it.get(position).calories)
                    mapIntent.putExtra(DISTANCE, it.get(position).distance)
                    mapIntent.putExtra(LOCATIONS, it.get(position).locations)
                    mapIntent.putExtra(StartFragment.TRACKON, false)

                    editor.apply()
                    startActivity(mapIntent)
                }


            }
        })
    }
}