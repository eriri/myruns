package com.example.myruns.data

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch

class EntryRepository(private val entryDao: EntryDao) {

    val allEntries: Flow<List<Entry>> = entryDao.getAllEntries()

    fun getEntry(id: Long): Flow<Entry> = entryDao.getEntry(id)

    fun insert(entry: Entry) {
        CoroutineScope(Dispatchers.IO).launch {
            entryDao.insertEntry(entry)
        }
    }

    fun delete(id: Long) {
        CoroutineScope(Dispatchers.IO).launch {
            entryDao.deleteEntry(id)
        }
    }

    fun deleteAll() {
        CoroutineScope(Dispatchers.IO).launch {
            entryDao.deleteAll()
        }
    }

}