package com.example.myruns.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "entries")
data class Entry(
    @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "id") var entryId: Long = 0L,
    var entryType: String = "",
    var activityType: String = "",
    var dataTime: String = "",
    var duration: Float = 0F,
    var distance: Float = 0F,
    var calories: Int = 0,
    var heartRate: Int = 0,
    var comment: String = "",
    var avgSpeed: Float = 0F,
    var climb: Float = 0F,
    var locations: String = ""
)