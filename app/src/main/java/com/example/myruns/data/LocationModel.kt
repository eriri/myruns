package com.example.myruns.data

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class LocationModel(
    val longitude: Double,
    val latitude: Double,
    val avgSpeed: Double,
    val curSpeed: Double,
    val climb: Double,
    val calories: Double,
    val distance: Double
) : Parcelable