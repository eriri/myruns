package com.example.myruns.data;

public class WekaClassifier {

    public static double classify(Object[] i)
            throws Exception {

        double p = Double.NaN;
        p = WekaClassifier.N593493380(i);
        return p;
    }
    static double N593493380(Object []i) {
        double p = Double.NaN;
        if (i[0] == null) {
            p = 0;
        } else if (((Double) i[0]).doubleValue() <= 64.727755) {
            p = WekaClassifier.N259917f21(i);
        } else if (((Double) i[0]).doubleValue() > 64.727755) {
            p = WekaClassifier.N35a8c2c26(i);
        }
        return p;
    }
    static double N259917f21(Object []i) {
        double p = Double.NaN;
        if (i[64] == null) {
            p = 0;
        } else if (((Double) i[64]).doubleValue() <= 1.000748) {
            p = 0;
        } else if (((Double) i[64]).doubleValue() > 1.000748) {
            p = WekaClassifier.N48885e5a2(i);
        }
        return p;
    }
    static double N48885e5a2(Object []i) {
        double p = Double.NaN;
        if (i[5] == null) {
            p = 0;
        } else if (((Double) i[5]).doubleValue() <= 2.094698) {
            p = WekaClassifier.N2fc371593(i);
        } else if (((Double) i[5]).doubleValue() > 2.094698) {
            p = 0;
        }
        return p;
    }
    static double N2fc371593(Object []i) {
        double p = Double.NaN;
        if (i[25] == null) {
            p = 0;
        } else if (((Double) i[25]).doubleValue() <= 0.539344) {
            p = WekaClassifier.N52939aca4(i);
        } else if (((Double) i[25]).doubleValue() > 0.539344) {
            p = 1;
        }
        return p;
    }
    static double N52939aca4(Object []i) {
        double p = Double.NaN;
        if (i[15] == null) {
            p = 1;
        } else if (((Double) i[15]).doubleValue() <= 0.315945) {
            p = WekaClassifier.N3fd831ed5(i);
        } else if (((Double) i[15]).doubleValue() > 0.315945) {
            p = 0;
        }
        return p;
    }
    static double N3fd831ed5(Object []i) {
        double p = Double.NaN;
        if (i[9] == null) {
            p = 0;
        } else if (((Double) i[9]).doubleValue() <= 0.534687) {
            p = 0;
        } else if (((Double) i[9]).doubleValue() > 0.534687) {
            p = 1;
        }
        return p;
    }
    static double N35a8c2c26(Object []i) {
        double p = Double.NaN;
        if (i[64] == null) {
            p = 1;
        } else if (((Double) i[64]).doubleValue() <= 10.115397) {
            p = WekaClassifier.N15534977(i);
        } else if (((Double) i[64]).doubleValue() > 10.115397) {
            p = WekaClassifier.N1a3334931(i);
        }
        return p;
    }
    static double N15534977(Object []i) {
        double p = Double.NaN;
        if (i[64] == null) {
            p = 1;
        } else if (((Double) i[64]).doubleValue() <= 7.439456) {
            p = WekaClassifier.N4fdeabbd8(i);
        } else if (((Double) i[64]).doubleValue() > 7.439456) {
            p = WekaClassifier.N3cfe7e1b16(i);
        }
        return p;
    }
    static double N4fdeabbd8(Object []i) {
        double p = Double.NaN;
        if (i[0] == null) {
            p = 1;
        } else if (((Double) i[0]).doubleValue() <= 98.96572) {
            p = WekaClassifier.N3177acf29(i);
        } else if (((Double) i[0]).doubleValue() > 98.96572) {
            p = WekaClassifier.N6a63b38011(i);
        }
        return p;
    }
    static double N3177acf29(Object []i) {
        double p = Double.NaN;
        if (i[12] == null) {
            p = 0;
        } else if (((Double) i[12]).doubleValue() <= 0.477334) {
            p = 0;
        } else if (((Double) i[12]).doubleValue() > 0.477334) {
            p = WekaClassifier.N42bad6cc10(i);
        }
        return p;
    }
    static double N42bad6cc10(Object []i) {
        double p = Double.NaN;
        if (i[32] == null) {
            p = 0;
        } else if (((Double) i[32]).doubleValue() <= 0.009676) {
            p = 0;
        } else if (((Double) i[32]).doubleValue() > 0.009676) {
            p = 1;
        }
        return p;
    }
    static double N6a63b38011(Object []i) {
        double p = Double.NaN;
        if (i[5] == null) {
            p = 1;
        } else if (((Double) i[5]).doubleValue() <= 12.779909) {
            p = WekaClassifier.N505a6e3212(i);
        } else if (((Double) i[5]).doubleValue() > 12.779909) {
            p = 1;
        }
        return p;
    }
    static double N505a6e3212(Object []i) {
        double p = Double.NaN;
        if (i[10] == null) {
            p = 1;
        } else if (((Double) i[10]).doubleValue() <= 4.065571) {
            p = 1;
        } else if (((Double) i[10]).doubleValue() > 4.065571) {
            p = WekaClassifier.N5c4648f713(i);
        }
        return p;
    }
    static double N5c4648f713(Object []i) {
        double p = Double.NaN;
        if (i[9] == null) {
            p = 2;
        } else if (((Double) i[9]).doubleValue() <= 5.312216) {
            p = WekaClassifier.N485ef90514(i);
        } else if (((Double) i[9]).doubleValue() > 5.312216) {
            p = 1;
        }
        return p;
    }
    static double N485ef90514(Object []i) {
        double p = Double.NaN;
        if (i[13] == null) {
            p = 1;
        } else if (((Double) i[13]).doubleValue() <= 3.476813) {
            p = WekaClassifier.N3f56bc3d15(i);
        } else if (((Double) i[13]).doubleValue() > 3.476813) {
            p = 2;
        }
        return p;
    }
    static double N3f56bc3d15(Object []i) {
        double p = Double.NaN;
        if (i[6] == null) {
            p = 2;
        } else if (((Double) i[6]).doubleValue() <= 6.791068) {
            p = 2;
        } else if (((Double) i[6]).doubleValue() > 6.791068) {
            p = 1;
        }
        return p;
    }
    static double N3cfe7e1b16(Object []i) {
        double p = Double.NaN;
        if (i[6] == null) {
            p = 1;
        } else if (((Double) i[6]).doubleValue() <= 12.78048) {
            p = WekaClassifier.N5291888317(i);
        } else if (((Double) i[6]).doubleValue() > 12.78048) {
            p = WekaClassifier.N7196c8ec30(i);
        }
        return p;
    }
    static double N5291888317(Object []i) {
        double p = Double.NaN;
        if (i[15] == null) {
            p = 1;
        } else if (((Double) i[15]).doubleValue() <= 3.582097) {
            p = WekaClassifier.N6db8c50518(i);
        } else if (((Double) i[15]).doubleValue() > 3.582097) {
            p = WekaClassifier.N43b5e2ba28(i);
        }
        return p;
    }
    static double N6db8c50518(Object []i) {
        double p = Double.NaN;
        if (i[4] == null) {
            p = 1;
        } else if (((Double) i[4]).doubleValue() <= 7.73927) {
            p = WekaClassifier.N67053bdb19(i);
        } else if (((Double) i[4]).doubleValue() > 7.73927) {
            p = WekaClassifier.N76cfcf8d21(i);
        }
        return p;
    }
    static double N67053bdb19(Object []i) {
        double p = Double.NaN;
        if (i[6] == null) {
            p = 1;
        } else if (((Double) i[6]).doubleValue() <= 6.820282) {
            p = WekaClassifier.N28c48f8620(i);
        } else if (((Double) i[6]).doubleValue() > 6.820282) {
            p = 2;
        }
        return p;
    }
    static double N28c48f8620(Object []i) {
        double p = Double.NaN;
        if (i[16] == null) {
            p = 1;
        } else if (((Double) i[16]).doubleValue() <= 2.651553) {
            p = 1;
        } else if (((Double) i[16]).doubleValue() > 2.651553) {
            p = 2;
        }
        return p;
    }
    static double N76cfcf8d21(Object []i) {
        double p = Double.NaN;
        if (i[0] == null) {
            p = 1;
        } else if (((Double) i[0]).doubleValue() <= 249.114125) {
            p = 1;
        } else if (((Double) i[0]).doubleValue() > 249.114125) {
            p = WekaClassifier.N5d74acbc22(i);
        }
        return p;
    }
    static double N5d74acbc22(Object []i) {
        double p = Double.NaN;
        if (i[19] == null) {
            p = 2;
        } else if (((Double) i[19]).doubleValue() <= 0.242952) {
            p = 2;
        } else if (((Double) i[19]).doubleValue() > 0.242952) {
            p = WekaClassifier.N13618f2a23(i);
        }
        return p;
    }
    static double N13618f2a23(Object []i) {
        double p = Double.NaN;
        if (i[14] == null) {
            p = 1;
        } else if (((Double) i[14]).doubleValue() <= 1.054018) {
            p = WekaClassifier.N1fd58a7f24(i);
        } else if (((Double) i[14]).doubleValue() > 1.054018) {
            p = WekaClassifier.N6f87d2a226(i);
        }
        return p;
    }
    static double N1fd58a7f24(Object []i) {
        double p = Double.NaN;
        if (i[64] == null) {
            p = 1;
        } else if (((Double) i[64]).doubleValue() <= 8.873593) {
            p = 1;
        } else if (((Double) i[64]).doubleValue() > 8.873593) {
            p = WekaClassifier.N17ee9daf25(i);
        }
        return p;
    }
    static double N17ee9daf25(Object []i) {
        double p = Double.NaN;
        if (i[3] == null) {
            p = 2;
        } else if (((Double) i[3]).doubleValue() <= 18.179738) {
            p = 2;
        } else if (((Double) i[3]).doubleValue() > 18.179738) {
            p = 1;
        }
        return p;
    }
    static double N6f87d2a226(Object []i) {
        double p = Double.NaN;
        if (i[20] == null) {
            p = 2;
        } else if (((Double) i[20]).doubleValue() <= 1.023386) {
            p = 2;
        } else if (((Double) i[20]).doubleValue() > 1.023386) {
            p = WekaClassifier.N708008a727(i);
        }
        return p;
    }
    static double N708008a727(Object []i) {
        double p = Double.NaN;
        if (i[30] == null) {
            p = 1;
        } else if (((Double) i[30]).doubleValue() <= 2.363673) {
            p = 1;
        } else if (((Double) i[30]).doubleValue() > 2.363673) {
            p = 2;
        }
        return p;
    }
    static double N43b5e2ba28(Object []i) {
        double p = Double.NaN;
        if (i[1] == null) {
            p = 1;
        } else if (((Double) i[1]).doubleValue() <= 101.011113) {
            p = 1;
        } else if (((Double) i[1]).doubleValue() > 101.011113) {
            p = WekaClassifier.N20550c8629(i);
        }
        return p;
    }
    static double N20550c8629(Object []i) {
        double p = Double.NaN;
        if (i[2] == null) {
            p = 2;
        } else if (((Double) i[2]).doubleValue() <= 24.764079) {
            p = 2;
        } else if (((Double) i[2]).doubleValue() > 24.764079) {
            p = 1;
        }
        return p;
    }
    static double N7196c8ec30(Object []i) {
        double p = Double.NaN;
        if (i[64] == null) {
            p = 2;
        } else if (((Double) i[64]).doubleValue() <= 9.528724) {
            p = 2;
        } else if (((Double) i[64]).doubleValue() > 9.528724) {
            p = 1;
        }
        return p;
    }
    static double N1a3334931(Object []i) {
        double p = Double.NaN;
        if (i[2] == null) {
            p = 2;
        } else if (((Double) i[2]).doubleValue() <= 89.037609) {
            p = WekaClassifier.N6defb1f532(i);
        } else if (((Double) i[2]).doubleValue() > 89.037609) {
            p = 2;
        }
        return p;
    }
    static double N6defb1f532(Object []i) {
        double p = Double.NaN;
        if (i[32] == null) {
            p = 2;
        } else if (((Double) i[32]).doubleValue() <= 4.367182) {
            p = WekaClassifier.N65d6b1f233(i);
        } else if (((Double) i[32]).doubleValue() > 4.367182) {
            p = WekaClassifier.N694175df58(i);
        }
        return p;
    }
    static double N65d6b1f233(Object []i) {
        double p = Double.NaN;
        if (i[5] == null) {
            p = 2;
        } else if (((Double) i[5]).doubleValue() <= 23.274028) {
            p = WekaClassifier.N22c0c40c34(i);
        } else if (((Double) i[5]).doubleValue() > 23.274028) {
            p = 2;
        }
        return p;
    }
    static double N22c0c40c34(Object []i) {
        double p = Double.NaN;
        if (i[2] == null) {
            p = 2;
        } else if (((Double) i[2]).doubleValue() <= 57.966233) {
            p = WekaClassifier.N3a61926635(i);
        } else if (((Double) i[2]).doubleValue() > 57.966233) {
            p = WekaClassifier.N6232dfb152(i);
        }
        return p;
    }
    static double N3a61926635(Object []i) {
        double p = Double.NaN;
        if (i[23] == null) {
            p = 2;
        } else if (((Double) i[23]).doubleValue() <= 2.515828) {
            p = WekaClassifier.N2a11ecc636(i);
        } else if (((Double) i[23]).doubleValue() > 2.515828) {
            p = WekaClassifier.N7b05b1e744(i);
        }
        return p;
    }
    static double N2a11ecc636(Object []i) {
        double p = Double.NaN;
        if (i[6] == null) {
            p = 2;
        } else if (((Double) i[6]).doubleValue() <= 8.468014) {
            p = WekaClassifier.N7a98915737(i);
        } else if (((Double) i[6]).doubleValue() > 8.468014) {
            p = 2;
        }
        return p;
    }
    static double N7a98915737(Object []i) {
        double p = Double.NaN;
        if (i[6] == null) {
            p = 1;
        } else if (((Double) i[6]).doubleValue() <= 1.363133) {
            p = 1;
        } else if (((Double) i[6]).doubleValue() > 1.363133) {
            p = WekaClassifier.N5edccaac38(i);
        }
        return p;
    }
    static double N5edccaac38(Object []i) {
        double p = Double.NaN;
        if (i[3] == null) {
            p = 2;
        } else if (((Double) i[3]).doubleValue() <= 33.610028) {
            p = WekaClassifier.N681891b339(i);
        } else if (((Double) i[3]).doubleValue() > 33.610028) {
            p = 2;
        }
        return p;
    }
    static double N681891b339(Object []i) {
        double p = Double.NaN;
        if (i[21] == null) {
            p = 2;
        } else if (((Double) i[21]).doubleValue() <= 1.258501) {
            p = WekaClassifier.N4dca37aa40(i);
        } else if (((Double) i[21]).doubleValue() > 1.258501) {
            p = 1;
        }
        return p;
    }
    static double N4dca37aa40(Object []i) {
        double p = Double.NaN;
        if (i[17] == null) {
            p = 2;
        } else if (((Double) i[17]).doubleValue() <= 1.322024) {
            p = WekaClassifier.N73e6156d41(i);
        } else if (((Double) i[17]).doubleValue() > 1.322024) {
            p = 2;
        }
        return p;
    }
    static double N73e6156d41(Object []i) {
        double p = Double.NaN;
        if (i[6] == null) {
            p = 2;
        } else if (((Double) i[6]).doubleValue() <= 5.333629) {
            p = WekaClassifier.N4bd7281742(i);
        } else if (((Double) i[6]).doubleValue() > 5.333629) {
            p = 2;
        }
        return p;
    }
    static double N4bd7281742(Object []i) {
        double p = Double.NaN;
        if (i[1] == null) {
            p = 2;
        } else if (((Double) i[1]).doubleValue() <= 126.953491) {
            p = WekaClassifier.N67eebdd743(i);
        } else if (((Double) i[1]).doubleValue() > 126.953491) {
            p = 1;
        }
        return p;
    }
    static double N67eebdd743(Object []i) {
        double p = Double.NaN;
        if (i[2] == null) {
            p = 1;
        } else if (((Double) i[2]).doubleValue() <= 13.221267) {
            p = 1;
        } else if (((Double) i[2]).doubleValue() > 13.221267) {
            p = 2;
        }
        return p;
    }
    static double N7b05b1e744(Object []i) {
        double p = Double.NaN;
        if (i[7] == null) {
            p = 1;
        } else if (((Double) i[7]).doubleValue() <= 14.834488) {
            p = WekaClassifier.N388b8e9745(i);
        } else if (((Double) i[7]).doubleValue() > 14.834488) {
            p = 2;
        }
        return p;
    }
    static double N388b8e9745(Object []i) {
        double p = Double.NaN;
        if (i[3] == null) {
            p = 1;
        } else if (((Double) i[3]).doubleValue() <= 27.007047) {
            p = WekaClassifier.N4aee9e0146(i);
        } else if (((Double) i[3]).doubleValue() > 27.007047) {
            p = WekaClassifier.N587c928e47(i);
        }
        return p;
    }
    static double N4aee9e0146(Object []i) {
        double p = Double.NaN;
        if (i[2] == null) {
            p = 2;
        } else if (((Double) i[2]).doubleValue() <= 15.289488) {
            p = 2;
        } else if (((Double) i[2]).doubleValue() > 15.289488) {
            p = 1;
        }
        return p;
    }
    static double N587c928e47(Object []i) {
        double p = Double.NaN;
        if (i[1] == null) {
            p = 2;
        } else if (((Double) i[1]).doubleValue() <= 68.259513) {
            p = 2;
        } else if (((Double) i[1]).doubleValue() > 68.259513) {
            p = WekaClassifier.Nea08aef48(i);
        }
        return p;
    }
    static double Nea08aef48(Object []i) {
        double p = Double.NaN;
        if (i[64] == null) {
            p = 1;
        } else if (((Double) i[64]).doubleValue() <= 12.340058) {
            p = 1;
        } else if (((Double) i[64]).doubleValue() > 12.340058) {
            p = WekaClassifier.N6abb529a49(i);
        }
        return p;
    }
    static double N6abb529a49(Object []i) {
        double p = Double.NaN;
        if (i[26] == null) {
            p = 1;
        } else if (((Double) i[26]).doubleValue() <= 2.777286) {
            p = 1;
        } else if (((Double) i[26]).doubleValue() > 2.777286) {
            p = WekaClassifier.N549bd75a50(i);
        }
        return p;
    }
    static double N549bd75a50(Object []i) {
        double p = Double.NaN;
        if (i[1] == null) {
            p = 2;
        } else if (((Double) i[1]).doubleValue() <= 125.270071) {
            p = 2;
        } else if (((Double) i[1]).doubleValue() > 125.270071) {
            p = WekaClassifier.N684fd7a551(i);
        }
        return p;
    }
    static double N684fd7a551(Object []i) {
        double p = Double.NaN;
        if (i[4] == null) {
            p = 2;
        } else if (((Double) i[4]).doubleValue() <= 18.532817) {
            p = 2;
        } else if (((Double) i[4]).doubleValue() > 18.532817) {
            p = 1;
        }
        return p;
    }
    static double N6232dfb152(Object []i) {
        double p = Double.NaN;
        if (i[28] == null) {
            p = 2;
        } else if (((Double) i[28]).doubleValue() <= 3.47645) {
            p = WekaClassifier.N52ad6eef53(i);
        } else if (((Double) i[28]).doubleValue() > 3.47645) {
            p = WekaClassifier.N3ba0e3557(i);
        }
        return p;
    }
    static double N52ad6eef53(Object []i) {
        double p = Double.NaN;
        if (i[1] == null) {
            p = 2;
        } else if (((Double) i[1]).doubleValue() <= 95.43085) {
            p = WekaClassifier.N580642e754(i);
        } else if (((Double) i[1]).doubleValue() > 95.43085) {
            p = 2;
        }
        return p;
    }
    static double N580642e754(Object []i) {
        double p = Double.NaN;
        if (i[0] == null) {
            p = 2;
        } else if (((Double) i[0]).doubleValue() <= 873.264223) {
            p = WekaClassifier.N4ee254bd55(i);
        } else if (((Double) i[0]).doubleValue() > 873.264223) {
            p = 1;
        }
        return p;
    }
    static double N4ee254bd55(Object []i) {
        double p = Double.NaN;
        if (i[3] == null) {
            p = 1;
        } else if (((Double) i[3]).doubleValue() <= 11.460913) {
            p = WekaClassifier.N3a59d58756(i);
        } else if (((Double) i[3]).doubleValue() > 11.460913) {
            p = 2;
        }
        return p;
    }
    static double N3a59d58756(Object []i) {
        double p = Double.NaN;
        if (i[7] == null) {
            p = 2;
        } else if (((Double) i[7]).doubleValue() <= 3.553115) {
            p = 2;
        } else if (((Double) i[7]).doubleValue() > 3.553115) {
            p = 1;
        }
        return p;
    }
    static double N3ba0e3557(Object []i) {
        double p = Double.NaN;
        if (i[8] == null) {
            p = 1;
        } else if (((Double) i[8]).doubleValue() <= 10.182156) {
            p = 1;
        } else if (((Double) i[8]).doubleValue() > 10.182156) {
            p = 2;
        }
        return p;
    }
    static double N694175df58(Object []i) {
        double p = Double.NaN;
        if (i[1] == null) {
            p = 2;
        } else if (((Double) i[1]).doubleValue() <= 64.729776) {
            p = 2;
        } else if (((Double) i[1]).doubleValue() > 64.729776) {
            p = WekaClassifier.N35da83f659(i);
        }
        return p;
    }
    static double N35da83f659(Object []i) {
        double p = Double.NaN;
        if (i[64] == null) {
            p = 1;
        } else if (((Double) i[64]).doubleValue() <= 17.673525) {
            p = WekaClassifier.N415b8d2760(i);
        } else if (((Double) i[64]).doubleValue() > 17.673525) {
            p = WekaClassifier.N14fe3f165(i);
        }
        return p;
    }
    static double N415b8d2760(Object []i) {
        double p = Double.NaN;
        if (i[4] == null) {
            p = 1;
        } else if (((Double) i[4]).doubleValue() <= 38.967938) {
            p = WekaClassifier.N12c3f89f61(i);
        } else if (((Double) i[4]).doubleValue() > 38.967938) {
            p = WekaClassifier.Ncc5f5f664(i);
        }
        return p;
    }
    static double N12c3f89f61(Object []i) {
        double p = Double.NaN;
        if (i[4] == null) {
            p = 2;
        } else if (((Double) i[4]).doubleValue() <= 17.732979) {
            p = 2;
        } else if (((Double) i[4]).doubleValue() > 17.732979) {
            p = WekaClassifier.N704e889562(i);
        }
        return p;
    }
    static double N704e889562(Object []i) {
        double p = Double.NaN;
        if (i[3] == null) {
            p = 1;
        } else if (((Double) i[3]).doubleValue() <= 44.823978) {
            p = 1;
        } else if (((Double) i[3]).doubleValue() > 44.823978) {
            p = WekaClassifier.N11ea605963(i);
        }
        return p;
    }
    static double N11ea605963(Object []i) {
        double p = Double.NaN;
        if (i[2] == null) {
            p = 2;
        } else if (((Double) i[2]).doubleValue() <= 49.538809) {
            p = 2;
        } else if (((Double) i[2]).doubleValue() > 49.538809) {
            p = 1;
        }
        return p;
    }
    static double Ncc5f5f664(Object []i) {
        double p = Double.NaN;
        if (i[7] == null) {
            p = 2;
        } else if (((Double) i[7]).doubleValue() <= 19.439213) {
            p = 2;
        } else if (((Double) i[7]).doubleValue() > 19.439213) {
            p = 1;
        }
        return p;
    }
    static double N14fe3f165(Object []i) {
        double p = Double.NaN;
        if (i[7] == null) {
            p = 2;
        } else if (((Double) i[7]).doubleValue() <= 20.632754) {
            p = 2;
        } else if (((Double) i[7]).doubleValue() > 20.632754) {
            p = WekaClassifier.N5c1c96d66(i);
        }
        return p;
    }
    static double N5c1c96d66(Object []i) {
        double p = Double.NaN;
        if (i[5] == null) {
            p = 1;
        } else if (((Double) i[5]).doubleValue() <= 31.903804) {
            p = 1;
        } else if (((Double) i[5]).doubleValue() > 31.903804) {
            p = 2;
        }
        return p;
    }
}