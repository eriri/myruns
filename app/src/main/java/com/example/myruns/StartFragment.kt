package com.example.myruns

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.fragment.app.Fragment


class StartFragment : Fragment() {
    private lateinit var inputTypeAdapter: ArrayAdapter<CharSequence>
    private lateinit var activityAdapter: ArrayAdapter<CharSequence>

    companion object {
        const val INTTYPE = "input"
        const val ACTTYPE = "activity"
        const val TRACKON = "trackon"
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment_start, container, false)

        val inputSpinner: Spinner = view.findViewById(R.id.input_spinner)
        val activitySpinner: Spinner = view.findViewById(R.id.activity_spinner)

        inputTypeAdapter = ArrayAdapter.createFromResource(
            view.context,
            R.array.input_type,
            android.R.layout.simple_list_item_1
        )
        activityAdapter = ArrayAdapter.createFromResource(
            view.context,
            R.array.activity_type,
            android.R.layout.simple_list_item_1
        )

        inputSpinner.adapter = inputTypeAdapter
        activitySpinner.adapter = activityAdapter

        view.findViewById<View>(R.id.start_button).setOnClickListener(View.OnClickListener {
            val inputType = inputSpinner.selectedItem.toString()
            val activityType: String = activitySpinner.selectedItem.toString()
            if (inputType.lowercase() == "manual entry") {
                val manualInputIntent = Intent(activity, ManualListActivity::class.java)
                manualInputIntent.putExtra(INTTYPE, inputType)
                manualInputIntent.putExtra(ACTTYPE, activityType)
                startActivity(manualInputIntent)
            } else {
                val mapIntent = Intent(activity, MapsActivity::class.java)
                mapIntent.putExtra(INTTYPE, inputType)
                mapIntent.putExtra(ACTTYPE, activityType)
                mapIntent.putExtra(TRACKON, true)
                startActivity(mapIntent)
            }
        })
        return view
    }
}