package com.example.myruns

import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.activity.result.ActivityResult
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.FileProvider
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.myruns.viewmodels.MyViewModel
import com.soundcloud.android.crop.Crop
import java.io.File


class CameraDialog : DialogFragment() {
    private lateinit var myViewModel: MyViewModel
    private lateinit var tempImgUri: Uri
    private val tempImgFileName = "temp.jpg"

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        lateinit var ret: Dialog
        val builder = AlertDialog.Builder(requireActivity())
        val view: View =
            requireActivity().layoutInflater.inflate(R.layout.fragment_image_dialog, null)

        val openCamText: TextView = view.findViewById(R.id.open_camera)
        val selectGalleryText: TextView = view.findViewById(R.id.select_gallery)

        val tempImgFile = File(
            requireActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES),
            tempImgFileName
        )
        tempImgUri =
            FileProvider.getUriForFile(requireActivity(), "com.example.myruns", tempImgFile)

        myViewModel = ViewModelProvider(this).get(MyViewModel::class.java)
        myViewModel.userImage.observe(this, Observer { it ->
            requireActivity().findViewById<ImageView>(R.id.profile_image).setImageBitmap(it)
        })

        selectGalleryText.setOnClickListener {
            val intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
            intent.putExtra(MediaStore.EXTRA_OUTPUT, tempImgUri)
            pickResult.launch(intent)
        }

        openCamText.setOnClickListener {
            val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            intent.putExtra(MediaStore.EXTRA_OUTPUT, tempImgUri)
            cameraResult.launch(intent)
        }

        builder.setView(view)
        ret = builder.create()
        return ret
    }

    private val pickResult: ActivityResultLauncher<Intent> = registerForActivityResult(
        ActivityResultContracts.StartActivityForResult()
    ) { result: ActivityResult ->
        if (result.resultCode == AppCompatActivity.RESULT_OK) {
            val uri: Uri? = result.data?.getData()
            val cropIntent: Intent = Crop.of(uri, tempImgUri)
                .asSquare().getIntent(requireActivity())
            cropImage.launch(cropIntent)
        }
    }

    private val cameraResult: ActivityResultLauncher<Intent> = registerForActivityResult(
        ActivityResultContracts.StartActivityForResult()
    ) { result: ActivityResult ->
        if (result.resultCode == Activity.RESULT_OK) {
            val cropIntent: Intent = Crop.of(tempImgUri, tempImgUri)
                .asSquare().getIntent(requireActivity())
            cropImage.launch(cropIntent)
        }
    }

    private val cropImage: ActivityResultLauncher<Intent> = registerForActivityResult(
        ActivityResultContracts.StartActivityForResult()
    ) { result: ActivityResult ->
        if (result.resultCode == AppCompatActivity.RESULT_OK) {
            val bitmap = Utils.getBitmap(requireActivity(), tempImgUri)
            myViewModel.userImage.value = bitmap
            dialog!!.dismiss()
        }
    }
}
