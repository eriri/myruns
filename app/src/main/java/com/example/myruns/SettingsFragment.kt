package com.example.myruns

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Switch
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager


class SettingsFragment : Fragment() {
    private val PRIVPREF = "privpref";
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_settings, container, false)

        view.findViewById<View>(R.id.account_profile_group).setOnClickListener(
            View.OnClickListener {
                val profileIntent = Intent(context, ProfileActivity::class.java)
                startActivity(profileIntent)
            })

        val privacySwitch: Switch = view.findViewById(R.id.privacy_checkbox)
        val sharedPreferences =
            requireActivity().getSharedPreferences(PRIVPREF, Context.MODE_PRIVATE)

        privacySwitch.isChecked = sharedPreferences.getBoolean(PRIVPREF, false)
        privacySwitch.setOnClickListener {
            val editor = sharedPreferences.edit()
            editor.putBoolean(PRIVPREF, privacySwitch.isChecked)
            editor.apply()
        }

        view.findViewById<View>(R.id.unit_preference).setOnClickListener(
            View.OnClickListener {
                showUnitDialog()
            })

        view.findViewById<View>(R.id.comments).setOnClickListener(
            View.OnClickListener {
                showSettingsDialog(getString(R.string.comments_text))
            })

        view.findViewById<View>(R.id.webpage).setOnClickListener(
            View.OnClickListener {
                val browserIntent = Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse(getString(R.string.webpage_link))
                )
                startActivity(browserIntent)
            })

        return view
    }

    private fun showSettingsDialog(title: String) {
        val myDialog = MyDialog()
        val bundle = Bundle()
        bundle.putInt(MyDialog.DIALOG_KEY, MyDialog.COMMENTSSETTING_DIALOG)
        bundle.putString(MyDialog.TITLE_KEY, title)
        myDialog.arguments = bundle

        val supportFragmentManager: FragmentManager = childFragmentManager
        myDialog.show(supportFragmentManager, "my dialog")
    }

    private fun showUnitDialog() {
        val supportFragmentManager: FragmentManager = childFragmentManager

        val unitDialog = UnitDialog()
        unitDialog.show(supportFragmentManager, "unit dialog")
    }
}
