package com.example.myruns

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Context
import android.os.Bundle
import android.os.PersistableBundle
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.example.myruns.data.AppDatabase
import com.example.myruns.data.Entry
import com.example.myruns.data.EntryDao
import com.example.myruns.data.EntryRepository
import com.example.myruns.viewmodels.EntryViewModel
import com.example.myruns.viewmodels.EntryViewModelFactory
import java.util.*


class ManualListActivity : AppCompatActivity(), TimePickerDialog.OnTimeSetListener,
    DatePickerDialog.OnDateSetListener {

    private lateinit var database: AppDatabase
    private lateinit var databaseDao: EntryDao
    private lateinit var repository: EntryRepository

    private lateinit var viewModelFactory: EntryViewModelFactory
    private lateinit var entryViewModel: EntryViewModel

    private val DETAILS = arrayOf(
        "Date", "Time", "Duration", "Distance", "Calories", "Heart Rate", "Comment"
    )
    private lateinit var myListView: ListView
    private var calendar = Calendar.getInstance()

    private lateinit var activityType: String
    private lateinit var inputType: String

    private lateinit var date: String
    private lateinit var time: String

    private lateinit var entry: Entry
    private val ENTRYPREFS = "entryprefs"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_manual_entry)

        database = AppDatabase.getInstance(this)
        databaseDao = database.entryDao
        repository = EntryRepository(databaseDao)

        viewModelFactory = EntryViewModelFactory(repository)
        entryViewModel = ViewModelProvider(this, viewModelFactory).get(EntryViewModel::class.java)

        activityType = intent.getStringExtra(StartFragment.ACTTYPE).toString()
        inputType = intent.getStringExtra(StartFragment.INTTYPE).toString()

        entry = Entry()
        entry.activityType = activityType
        entry.entryType = inputType

        time = String.format("%d", calendar.get(Calendar.HOUR_OF_DAY)) + ":" + String.format(
            "%02d",
            calendar.get(Calendar.MINUTE)
        )
        date = String.format("%d", calendar.get(Calendar.YEAR)) + "-" + String.format(
            "%02d",
            calendar.get(Calendar.MONTH) + 1
        ) + "-" + String.format("%02d", calendar.get(Calendar.DAY_OF_MONTH))

        myListView = findViewById(R.id.manual_list)

        val arrayAdapter: ArrayAdapter<String> =
            ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, DETAILS)
        myListView.adapter = arrayAdapter
        myListView.setOnItemClickListener() { parent, view, position, id ->

            if (position == 0) {
                showDateDialog()
            } else if (position == 1) {
                showTimeDialog()

            } else if (position == 2 || position == 3) {
                showNumDialog(DETAILS[position], MyDialog.DEC_DIALOG)
            } else if (position == 4 || position == 5) {
                showNumDialog(DETAILS[position], MyDialog.NUM_DIALOG)
            } else if (position == 6) {
                showNumDialog(DETAILS[position], MyDialog.COMMENT_DIALOG)
            }
        }
    }

    private fun showTimeDialog() {
        val timeListener = TimePickerDialog.OnTimeSetListener { timePicker, hourOfDay, minute ->
            time = String.format("%d", hourOfDay) + ":" + String.format("%02d", minute)
        }
        val timePickerDialog = TimePickerDialog(
            this, timeListener,
            calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), true
        )
        timePickerDialog.show()
    }

    private fun showDateDialog() {
        val dateListner =
            DatePickerDialog.OnDateSetListener { datePicker, year, month, dayOfMonth ->
                date = String.format("%d", year) + "-" + String.format(
                    "%02d",
                    month + 1
                ) + "-" + String.format("%02d", dayOfMonth)
            }
        val datePickerDialog = DatePickerDialog(
            this, dateListner, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
            calendar.get(Calendar.DAY_OF_MONTH)
        )
        datePickerDialog.show()
    }

    private fun showNumDialog(title: String, dialogKey: Int) {
        val myDialog = MyDialog()
        val bundle = Bundle()
        bundle.putInt(MyDialog.DIALOG_KEY, dialogKey)
        bundle.putString(MyDialog.TITLE_KEY, title)
        myDialog.arguments = bundle
        myDialog.show(supportFragmentManager, "my dialog")
    }

    fun saveManual(view: View) {
        val unitPreferences = getSharedPreferences(UnitDialog.UNITPREF, Context.MODE_PRIVATE)
        val unitID = unitPreferences?.getInt(UnitDialog.UNITPREF, 0)

        val sharedPreferences = getSharedPreferences(ENTRYPREFS, MODE_PRIVATE)

        entry.dataTime = "$date $time"
        entry.duration = sharedPreferences?.getFloat(DETAILS[2], 0F)!!

        var dist = sharedPreferences.getFloat(DETAILS[3], 0F)

        if (unitID == R.id.imperial_option) {
            dist *= 1.609F
        }

        entry.distance = sharedPreferences.getFloat(DETAILS[3], 0F)
        entry.calories = sharedPreferences.getInt(DETAILS[4], 0)
        entry.heartRate = sharedPreferences.getInt(DETAILS[5], 0)

        entryViewModel.insert(entry)

        sharedPreferences.edit().clear().apply()
        finish()
    }

    fun cancelManual(view: View) {
        Toast.makeText(this, "Entry discarded.", Toast.LENGTH_LONG).show()
        val sharedPreferences = getSharedPreferences(ENTRYPREFS, MODE_PRIVATE)
        sharedPreferences.edit().clear().apply()
        finish()
    }

    override fun onTimeSet(p0: TimePicker?, p1: Int, p2: Int) {
        Toast.makeText(this, "Ok clicked.", Toast.LENGTH_LONG).show()
    }

    override fun onDateSet(p0: DatePicker?, p1: Int, p2: Int, p3: Int) {
        Toast.makeText(this, "Ok clicked.", Toast.LENGTH_LONG).show()
    }

}
