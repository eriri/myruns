package com.example.myruns

import android.content.Context
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.widget.ListView
import androidx.appcompat.app.AppCompatActivity
import com.example.myruns.adapters.DisplayArrayAdapter
import kotlin.math.floor
import kotlin.math.roundToInt


class DisplayEntryActivity : AppCompatActivity() {
    private val HISTPREF = "history"

    private val INTTYPE = "input"
    private val ACTTYPE = "activity"
    private val DATETIME = "datetime"
    private val DURATION = "duration"
    private val DISTANCE = "distance"
    private val CALORIES = "calories"
    private val HEARTRATE = "heartrate"

    private val DETAILS = arrayOf(
        "Input Type",
        "Activity Type",
        "Date and Time",
        "Duration",
        "Distance",
        "Calories",
        "Heart Rate"
    )

    private lateinit var unit: String
    private lateinit var durationString: String

    private lateinit var myListView: ListView
    private lateinit var arrayAdapter: DisplayArrayAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_display_entry)

        val sharedPreferences = getSharedPreferences(HISTPREF, Context.MODE_PRIVATE)

        val ipt = sharedPreferences.getString(INTTYPE, "")
        val atp = sharedPreferences.getString(ACTTYPE, "")
        val dt = sharedPreferences.getString(DATETIME, "")
        val du = sharedPreferences.getFloat(DURATION, 0F)
        var dis = sharedPreferences.getFloat(DISTANCE, 0F)
        val ca = sharedPreferences.getInt(CALORIES, 0)
        val hr = sharedPreferences.getInt(HEARTRATE, 0)

        val unitPreferences = getSharedPreferences(UnitDialog.UNITPREF, Context.MODE_PRIVATE)
        val unitID = unitPreferences?.getInt(UnitDialog.UNITPREF, 0)
        if (unitID == R.id.imperial_option) {
            dis /= 1.609F
            unit = "miles"
        } else {
            unit = "km"
        }

        val minuteVal = floor(du).toInt()
        val secondVal = ((du - floor(du)) * 60).roundToInt()
        if (secondVal == 0) {
            durationString = "$minuteVal mins"
        } else {
            durationString = "$minuteVal mins $secondVal secs"
        }

        val entryValues: Array<String?> = arrayOf(
            ipt,
            atp,
            dt,
            durationString,
            String.format("%.2f", dis) + " " + unit,
            "$ca calories",
            "$hr bpm"
        )

        myListView = findViewById(R.id.display_list)
        arrayAdapter = DisplayArrayAdapter(this, DETAILS, entryValues)
        myListView.divider = null;
        myListView.adapter = arrayAdapter
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.history, menu)
        return true
    }

    fun deleteEntry(item: MenuItem) {
        val sharedPreferences = getSharedPreferences(HISTPREF, Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.putBoolean(HISTPREF, true)
        editor.apply()
        finish()
    }
}