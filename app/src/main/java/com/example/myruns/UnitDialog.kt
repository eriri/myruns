package com.example.myruns

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context.MODE_PRIVATE
import android.os.Bundle
import android.view.View
import android.widget.RadioGroup
import androidx.fragment.app.DialogFragment


class UnitDialog : DialogFragment() {
    companion object {
        const val UNITPREF = "unit"
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        lateinit var ret: Dialog
        val builder = AlertDialog.Builder(requireActivity())
        val view: View =
            requireActivity().layoutInflater.inflate(R.layout.fragment_unit_dialog, null)
        val unit: RadioGroup = view.findViewById(R.id.unit_types)

        builder.setTitle(getString(R.string.unit_text))

        val sharedPreferences = requireActivity().getSharedPreferences(
            UNITPREF,
            MODE_PRIVATE
        )
        sharedPreferences?.getInt(UNITPREF, 0)?.let { unit.check(it) }

        unit.setOnCheckedChangeListener(RadioGroup.OnCheckedChangeListener { group, checkedId ->
            val editor = sharedPreferences.edit()
            unit.checkedRadioButtonId.let { editor.putInt(UNITPREF, it) }
            editor.apply()
        })

        builder.setNegativeButton("Cancel") { dialog, which ->
            dialog.dismiss()
        }

        builder.setView(view)
        ret = builder.create()
        return ret
    }
}