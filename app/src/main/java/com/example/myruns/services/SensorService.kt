package com.example.myruns.services

import android.app.Service
import android.content.Intent
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.os.*
import com.example.myruns.data.FFT
import com.example.myruns.data.WekaClassifier
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.util.concurrent.ArrayBlockingQueue


class SensorService : Service(), SensorEventListener {
    private lateinit var sensorManager: SensorManager
    private lateinit var mAccBuffer: ArrayBlockingQueue<Double>

    private lateinit var myBinder: MyBinder
    private var msgHandler: Handler? = null

    private lateinit var pastActivities: ArrayList<String>

    companion object {
        const val STANDING = "Standing"
        const val WALKING = "Walking"
        const val RUNNING = "Running"

        const val ACCELEROMETER_BLOCK_CAPACITY = 64
        const val ACCELEROMETER_BUFFER_CAPACITY = 2048

        val STR_KEY = "str key"
        val MSG_STR_VALUE = 2
    }

    override fun onCreate() {
        super.onCreate()
        sensorManager = getSystemService(SENSOR_SERVICE) as SensorManager
        mAccBuffer = ArrayBlockingQueue<Double>(ACCELEROMETER_BUFFER_CAPACITY)
        myBinder = MyBinder()
        msgHandler = null
        pastActivities = ArrayList()
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        val sensor = sensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION)
        sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_NORMAL)
        predict()
        return super.onStartCommand(intent, flags, startId)
    }

    override fun onDestroy() {
        super.onDestroy()
        stopSelf()
        sensorManager.unregisterListener(this)
    }

    inner class MyBinder : Binder() {
        fun setmsgHandler(msgHandler: Handler) {
            this@SensorService.msgHandler = msgHandler
        }
    }

    override fun onBind(intent: Intent?): IBinder? {
        println("debug: Service onBind() called")
        return myBinder
    }

    override fun onUnbind(intent: Intent?): Boolean {
        println("debug: Service onUnBind() called~~~")
        msgHandler = null
        return true
    }

    override fun onSensorChanged(event: SensorEvent) {
        if (event.sensor.type == Sensor.TYPE_LINEAR_ACCELERATION) {
            val m = Math.sqrt(
                (event.values[0] * event.values[0] + event.values[1] * event.values[1] + (event.values[2]
                        * event.values[2])).toDouble()
            )
            // Inserts the specified element into this queue if it is possible
            // to do so immediately without violating capacity restrictions,
            // returning true upon success and throwing an IllegalStateException
            // if no space is currently available. When using a
            // capacity-restricted queue, it is generally preferable to use
            // offer.
            try {
                mAccBuffer.add(m)
            } catch (e: IllegalStateException) {

                // Exception happens when reach the capacity.
                // Doubling the buffer. ListBlockingQueue has no such issue,
                // But generally has worse performance
                val newBuf = ArrayBlockingQueue<Double>(mAccBuffer.size * 2)
                mAccBuffer.drainTo(newBuf)
                mAccBuffer = newBuf
                mAccBuffer.add(m)
            }
        }
    }

    private fun predict() {
        var blockSize = 0
        val fft = FFT(ACCELEROMETER_BLOCK_CAPACITY)
        val accBlock = DoubleArray(ACCELEROMETER_BLOCK_CAPACITY)
        val im = DoubleArray(ACCELEROMETER_BLOCK_CAPACITY)

        CoroutineScope(Dispatchers.IO).launch {
            try {
                while (true) {
                    accBlock[blockSize++] = mAccBuffer.take().toDouble()
                    if (blockSize == ACCELEROMETER_BLOCK_CAPACITY) {
                        blockSize = 0

                        val max = accBlock.maxOrNull()
                        fft.fft(accBlock, im)

                        val featureVector = DoubleArray(ACCELEROMETER_BLOCK_CAPACITY + 1)
                        for (i in accBlock.indices) {
                            val mag = Math.sqrt(
                                accBlock.get(i) * accBlock.get(i) + im.get(i) * im.get(i)
                            )
                            featureVector.set(i, java.lang.Double.valueOf(mag))
                            im[i] = .0
                        }

                        if (max != null) {
                            featureVector.set(ACCELEROMETER_BLOCK_CAPACITY, max)
                        }

                        val classified = WekaClassifier.classify(featureVector.toTypedArray())
                        val currentActivityType = getType(classified.toInt())
                        pastActivities.add(currentActivityType)
                        val topActivityType = getTopActivity()

                        val tempHandler = msgHandler
                        if (tempHandler != null) {
                            val bundle = Bundle()
                            bundle.putString(STR_KEY, currentActivityType + "," + topActivityType)
                            val message: Message = tempHandler.obtainMessage()
                            message.data = bundle
                            message.what = MSG_STR_VALUE
                            tempHandler.sendMessage(message)
                        }
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    private fun getType(label: Int): String {
        when (label) {
            0 -> return STANDING
            1 -> return WALKING
            2 -> return RUNNING
            else -> return "Unknown"
        }
    }

    private fun getTopActivity(): String? {
        val maxOccurring = pastActivities.groupBy { it }.mapValues { it.value.size }
            .maxByOrNull { it.value }?.key
        return maxOccurring
    }

    override fun onAccuracyChanged(p0: Sensor?, p1: Int) {

    }
}