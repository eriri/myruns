package com.example.myruns.services

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.Context
import android.content.Intent
import android.location.Criteria
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.*
import android.util.Log
import android.widget.Toast
import androidx.core.app.NotificationCompat
import com.example.myruns.MapsActivity
import com.example.myruns.R
import com.example.myruns.StartFragment
import com.example.myruns.data.*
import com.google.android.gms.maps.model.LatLng
import java.lang.Double.NaN
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList


class TrackingService : Service(), LocationListener {
    private lateinit var notificationManager: NotificationManager
    val NOTIFICATION_ID = 777
    private lateinit var myBinder: MyBinder
    private val CHANNEL_ID = "notification channel"
    private var msgHandler: Handler? = null

    private lateinit var startTime: Date
    private var startAltitude = 0.0

    private var timeElapsed: Long = 0
    private var avgSpeed = 0.0
    private var latitude = 0.0
    private var longitude = 0.0
    private var curSpeed = 0.0
    private var climb = 0.0
    private var calories = 0.0
    private var distance = 0.0

    private val UNITCAL = 0.06
    private val UNITTIME = 4.0

    private var initialTrack = true

    private lateinit var locationManager: LocationManager

    private lateinit var database: AppDatabase
    private lateinit var databaseDao: EntryDao
    private lateinit var repository: EntryRepository
    private lateinit var entry: Entry

    private lateinit var activityType: String
    private lateinit var inputType: String
    private lateinit var date: String
    private lateinit var time: String

    private lateinit var locations: ArrayList<LatLng>

    companion object {
        val INFO_KEY = "info key"
        val MSG_INFO_VALUE = 7
    }

    override fun onCreate() {
        super.onCreate()
        Log.d("???", "Service onCreate() called")
        showNotification()
        myBinder = MyBinder()
        msgHandler = null
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        println("debug: Service onStartCommand() called everytime startService() is called; startId: $startId flags: $flags")

        database = AppDatabase.getInstance(this)
        databaseDao = database.entryDao
        repository = EntryRepository(databaseDao)

        locations = ArrayList()

        activityType = intent?.getStringExtra(StartFragment.ACTTYPE).toString()
        inputType = intent?.getStringExtra(StartFragment.INTTYPE).toString()

        entry = Entry()
        entry.entryType = inputType
        startLocationUpdate()

        return START_NOT_STICKY
    }

    override fun onBind(intent: Intent?): IBinder? {
        println("debug: Service onBind() called")
        return myBinder
    }

    override fun onUnbind(intent: Intent?): Boolean {
        println("debug: Service onUnBind() called~~~")
        msgHandler = null
        stopSelf()
        return true
    }

    override fun onDestroy() {
        super.onDestroy()
        println("debug: Service onDestroy")
        notificationManager.cancel(NOTIFICATION_ID)
        locationManager.removeUpdates(this)

        val sharedPreferences = getSharedPreferences(MapsActivity.SAVEPREF, Context.MODE_PRIVATE)
        val save = sharedPreferences.getBoolean(MapsActivity.SAVEPREF, false)
        val editor = sharedPreferences.edit()
        if (save) {
            if (inputType.lowercase() == "automatic") {
                entry.activityType = MapsActivity.ACTIVITYTYPE
            } else {
                entry.activityType = activityType
            }

            entry.distance = distance.toFloat()
            entry.duration = timeElapsed / 60F

            if (!avgSpeed.isNaN()){
                entry.avgSpeed = avgSpeed.toFloat()
            }
            if (!climb.isNaN()){
                entry.climb = climb.toFloat()
            }
            if (!calories.isNaN()){
                entry.calories = calories.toInt()
            }
            if (!distance.isNaN()){
                entry.distance = distance.toFloat()
            }

            var locationsString = ""
            for (i in 0 until locations.size) {
                val l: LatLng = locations.get(i)
                locationsString = locationsString + l.latitude + "," + l.longitude + ";"
            }
            entry.locations = locationsString
            repository.insert(entry)

            editor.putBoolean(MapsActivity.SAVEPREF, false)
            editor.apply()
        }

        stopSelf()
    }

    inner class MyBinder : Binder() {
        fun setmsgHandler(msgHandler: Handler) {
            this@TrackingService.msgHandler = msgHandler
        }
    }

    private fun showNotification() {
        val intent = Intent(this, MapsActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_SINGLE_TOP
        val pendingIntent = PendingIntent.getActivity(
            this, 0, intent,
            PendingIntent.FLAG_UPDATE_CURRENT
        )
        val notificationBuilder: NotificationCompat.Builder = NotificationCompat.Builder(
            this,
            CHANNEL_ID
        )
        notificationBuilder.setSmallIcon(R.drawable.ic_launcher_background)
        notificationBuilder.setContentTitle("MyRuns")
        notificationBuilder.setContentText("Recording your path now")
        notificationBuilder.setContentIntent(pendingIntent)
        val notification = notificationBuilder.build()
        notificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        val notificationChannel = NotificationChannel(
            CHANNEL_ID,
            "channel name",
            NotificationManager.IMPORTANCE_DEFAULT
        )
        notificationManager.createNotificationChannel(notificationChannel)
        notificationManager.notify(NOTIFICATION_ID, notification)
    }

    private fun startLocationUpdate() {
        time = String.format(
            "%d",
            Calendar.getInstance().get(Calendar.HOUR_OF_DAY)
        ) + ":" + String.format("%02d", Calendar.getInstance().get(Calendar.MINUTE))
        date = String.format(
            "%d",
            Calendar.getInstance().get(Calendar.YEAR)
        ) + "-" + String.format(
            "%02d",
            Calendar.getInstance().get(Calendar.MONTH) + 1
        ) + "-" + String.format("%02d", Calendar.getInstance().get(Calendar.DAY_OF_MONTH))

        entry.dataTime = "$date $time"
        startTime = Calendar.getInstance().time
        initLocationManager()

    }

    fun initLocationManager() {
        try {
            locationManager = getSystemService(LOCATION_SERVICE) as LocationManager
            val criteria = Criteria()
            criteria.accuracy = Criteria.ACCURACY_FINE
            val provider = locationManager.getBestProvider(criteria, true)
            val location = locationManager.getLastKnownLocation(provider!!)
            if (location != null)
                onLocationChanged(location)
            locationManager.requestLocationUpdates(provider, (UNITTIME * 1000).toLong(), 0f, this)
        } catch (e: SecurityException) {
        }
    }

    override fun onLocationChanged(location: Location) {
        if (initialTrack) {
            startAltitude = location.altitude
            initialTrack = false
        }
        latitude = location.latitude
        longitude = location.longitude
        locations.add(LatLng(latitude, longitude))

        timeElapsed =
            TimeUnit.MILLISECONDS.toSeconds(Calendar.getInstance().timeInMillis - startTime.time)
        curSpeed = location.speed.toDouble() * 3.6
        distance += location.speed.toDouble() * 0.001 * UNITTIME
        avgSpeed = distance / timeElapsed.toDouble()
        calories = distance * UNITCAL
        climb = (startAltitude - location.altitude) * 0.001

        val locationModel =
            LocationModel(longitude, latitude, avgSpeed, curSpeed, climb, calories, distance)

        val tempHandler = msgHandler
        if (tempHandler != null) {
            val bundle = Bundle()
            bundle.putParcelable(INFO_KEY, locationModel)
            val message: Message = tempHandler.obtainMessage()
            message.data = bundle
            message.what = MSG_INFO_VALUE
            tempHandler.sendMessage(message)
        }
    }

    override fun onProviderDisabled(provider: String) {}
    override fun onProviderEnabled(provider: String) {}
    override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {
        Toast.makeText(this, "onStatusChanged", Toast.LENGTH_LONG).show();
    }
}