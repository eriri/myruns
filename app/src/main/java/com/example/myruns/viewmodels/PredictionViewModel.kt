package com.example.myruns.viewmodels

import android.content.ComponentName
import android.content.ServiceConnection
import android.os.Handler
import android.os.IBinder
import android.os.Looper
import android.os.Message
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.myruns.services.SensorService

class PredictionViewModel : ViewModel(), ServiceConnection {
    private var myMessageHandler: PredictionMessageHandler =
        PredictionMessageHandler(Looper.getMainLooper())

    private val _activityType = MutableLiveData<String>()
    val activityType: LiveData<String>
        get() = _activityType

    override fun onServiceConnected(name: ComponentName, iBinder: IBinder) {
        println("debug: ViewModel: onServiceConnected() called; ComponentName: $name")
        val tempBinder = iBinder as SensorService.MyBinder
        tempBinder.setmsgHandler(myMessageHandler)
    }

    override fun onServiceDisconnected(name: ComponentName) {
        println("debug: Activity: onServiceDisconnected() called~~~")
    }

    inner class PredictionMessageHandler(looper: Looper) : Handler(looper) {
        override fun handleMessage(msg: Message) {
            if (msg.what == SensorService.MSG_STR_VALUE) {
                val bundle = msg.data
                _activityType.value = bundle.getString(SensorService.STR_KEY)
            }
        }
    }
}