package com.example.myruns.viewmodels

import android.content.ComponentName
import android.content.ServiceConnection
import android.os.Handler
import android.os.IBinder
import android.os.Looper
import android.os.Message
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.myruns.data.LocationModel
import com.example.myruns.services.TrackingService


class LocationViewModel : ViewModel(), ServiceConnection {
    private lateinit var myMessageHandler: MyMessageHandler

    init {
        myMessageHandler = MyMessageHandler(Looper.getMainLooper())
    }

    private val _locationInfo = MutableLiveData<LocationModel>()
    val locationInfo: LiveData<LocationModel>
        get() = _locationInfo

    override fun onServiceConnected(name: ComponentName, iBinder: IBinder) {
        println("debug: ViewModel: onServiceConnected() called; ComponentName: $name")
        val tempBinder = iBinder as TrackingService.MyBinder
        tempBinder.setmsgHandler(myMessageHandler)
    }

    override fun onServiceDisconnected(name: ComponentName) {
        println("debug: Activity: onServiceDisconnected() called~~~")
    }

    inner class MyMessageHandler(looper: Looper) : Handler(looper) {
        override fun handleMessage(msg: Message) {
            if (msg.what == TrackingService.MSG_INFO_VALUE) {
                val bundle = msg.data
                _locationInfo.value = bundle.getParcelable(TrackingService.INFO_KEY)
            }
        }
    }
}

