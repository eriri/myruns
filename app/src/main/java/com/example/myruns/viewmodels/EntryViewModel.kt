package com.example.myruns.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.asLiveData
import com.example.myruns.data.Entry
import com.example.myruns.data.EntryRepository


class EntryViewModel(private val repository: EntryRepository) : ViewModel() {

    val allEntriesLiveData: LiveData<List<Entry>> = repository.allEntries.asLiveData()

    fun insert(entry: Entry) {
        repository.insert(entry)
    }

    fun delete(position: Int) {
        val entryList = allEntriesLiveData.value
        val entryId = entryList?.get(position)?.entryId
        entryId?.let { repository.delete(it) }
    }

    fun deleteAll() {
        repository.deleteAll()
    }

    fun get(position: Int): LiveData<Entry>? {
        val entryList = allEntriesLiveData.value
        val entryId = entryList?.get(position)?.entryId
        val entry = entryId?.let { repository.getEntry(it).asLiveData() }
        return entry
    }
}

class EntryViewModelFactory(private val repository: EntryRepository) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(EntryViewModel::class.java))
            return EntryViewModel(repository) as T
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}
