package com.example.myruns

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.example.myruns.databinding.ActivityMapsBinding
import com.example.myruns.services.SensorService
import com.example.myruns.services.TrackingService
import com.example.myruns.viewmodels.LocationViewModel
import com.example.myruns.viewmodels.PredictionViewModel
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*


class MapsActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap
    private lateinit var binding: ActivityMapsBinding
    private var zoom = 17f
    private val ZOOM = "zoom"

    private lateinit var markerOptions: MarkerOptions
    private lateinit var polylineOptions: PolylineOptions
    private lateinit var polylines: ArrayList<Polyline>

    private var firstRecorded = false
    private val FIRSTRECORD = "firstrecord"
    private lateinit var startMarker: Marker
    private lateinit var curMarker: Marker

    private lateinit var serviceIntent: Intent
    private lateinit var sensorIntent: Intent

    private lateinit var calorieView: TextView
    private lateinit var typeView: TextView
    private lateinit var avgSpeedView: TextView
    private lateinit var climbView: TextView
    private lateinit var distanceView: TextView
    private lateinit var curSpeedView: TextView

    private lateinit var locationViewModel: LocationViewModel
    private lateinit var predictionViewModel: PredictionViewModel

    private var serviceStarted = false
    private var SERVICE_STATUS_KEY = "service_status_key"
    private var isBind = false
    private val BIND_STATUS_KEY = "bind_status_key"

    private var sensorStarted = false
    private var SENSOR_STATUS_KEY = "sensor_status_key"

    private lateinit var activityType: String
    private lateinit var inputType: String

    private lateinit var unit: String
    private lateinit var speedUnit: String

    private lateinit var locations: ArrayList<LatLng>

    private var isAuto = false

    companion object {
        const val SAVEPREF = "savepref"
        var ACTIVITYTYPE = "???"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMapsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        if (savedInstanceState != null) {
            isBind = savedInstanceState.getBoolean(BIND_STATUS_KEY)
            serviceStarted = savedInstanceState.getBoolean(SERVICE_STATUS_KEY)
            sensorStarted = savedInstanceState.getBoolean(SENSOR_STATUS_KEY)
            firstRecorded = savedInstanceState.getBoolean(FIRSTRECORD)
            zoom = savedInstanceState.getFloat(ZOOM)
        }

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        mapFragment.setRetainInstance(true)

        locationViewModel = ViewModelProvider(this).get(LocationViewModel::class.java)
        predictionViewModel = ViewModelProvider(this).get(PredictionViewModel::class.java)

        climbView = findViewById(R.id.climb_text)
        calorieView = findViewById(R.id.calories_text)
        typeView = findViewById(R.id.type_text)
        avgSpeedView = findViewById(R.id.avg_speed_text)
        curSpeedView = findViewById(R.id.cur_speed_text)
        distanceView = findViewById(R.id.distance_text)

        activityType = intent.getStringExtra(StartFragment.ACTTYPE).toString()
        inputType = intent.getStringExtra(StartFragment.INTTYPE).toString()

        if (!intent.getBooleanExtra(StartFragment.TRACKON, false)) {
            val buttons: LinearLayout = findViewById(R.id.buttons)
            buttons.visibility = View.INVISIBLE
            typeView.text = "Type: " + activityType
        } else if (inputType.lowercase() == "automatic") {
            isAuto = true
            typeView.text = "Type: ???"
        }
        else{
            typeView.text = "Type: " + activityType
        }

        val unitPreferences = getSharedPreferences(UnitDialog.UNITPREF, Context.MODE_PRIVATE)
        val unitID = unitPreferences?.getInt(UnitDialog.UNITPREF, 0)

        var dis = intent.getFloatExtra(HistoryFragment.DISTANCE, 0F)
        var avgSpeed = intent.getFloatExtra(HistoryFragment.AVGSPEED, 0F)
        var climb = intent.getFloatExtra(HistoryFragment.CLIMB, 0F)

        if (unitID == R.id.imperial_option) {
            dis /= 1.609F
            avgSpeed /= 1.609F
            climb /= 1.609F
            unit = "miles"
            speedUnit = "m/h"
        } else {
            unit = "km"
            speedUnit = "km/h"
        }

        avgSpeedView.text = "Avg speed: " + String.format("%.2f", avgSpeed) + " " + speedUnit
        climbView.text =
            "Climb: " + String.format("%.2f", climb) + " " + unit
        calorieView.text =
            "Calorie: " + intent.getIntExtra(HistoryFragment.CALORIES, 0).toString()
        distanceView.text = "Distance: " + String.format("%.2f", dis) + " " + unit

        locations = ArrayList()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putBoolean(BIND_STATUS_KEY, isBind)
        outState.putBoolean(SERVICE_STATUS_KEY, serviceStarted)
        outState.putBoolean(FIRSTRECORD, firstRecorded)
        outState.putBoolean(SENSOR_STATUS_KEY, sensorStarted)
        outState.putFloat(ZOOM, zoom)

        if (this::curMarker.isInitialized) {
            println("exiting & removing current marker!!!")
            curMarker.remove()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.history, menu)
        return !intent.getBooleanExtra(StartFragment.TRACKON, false)
    }

    fun deleteEntry(item: MenuItem) {
        val sharedPreferences = getSharedPreferences(HistoryFragment.HISTPREF, Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.putBoolean(HistoryFragment.HISTPREF, true)
        editor.apply()
        finish()
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {

        mMap = googleMap
        mMap.mapType = GoogleMap.MAP_TYPE_NORMAL

        polylineOptions = PolylineOptions()
        polylineOptions.color(Color.BLACK)
        polylines = ArrayList()
        markerOptions = MarkerOptions()

        if (intent.getBooleanExtra(StartFragment.TRACKON, false)) {
            if (isAuto) autoDetection()
            updateCurrentLocations()
        } else {
            loadLocationHistory()
        }
    }

    private fun updateCurrentLocations() {
        serviceIntent = Intent(this, TrackingService::class.java)
        serviceIntent.putExtra(StartFragment.ACTTYPE, activityType)
        serviceIntent.putExtra(StartFragment.INTTYPE, inputType)

        if (!serviceStarted) {
            startService(serviceIntent)
            serviceStarted = true
        }

        bindService(serviceIntent, locationViewModel, Context.BIND_AUTO_CREATE)
        isBind = true

        locationViewModel.locationInfo.observe(this, {

            val latLng = LatLng(it.latitude, it.longitude)
            var dAvgSpeed = it.avgSpeed
            var dCurSpeed = it.curSpeed
            var dClimb = it.climb
            var dDistance = it.distance

            if (unit == "miles") {
                dAvgSpeed /= 1.609F
                dCurSpeed /= 1.609F
                dAvgSpeed /= 1.609F
                dClimb /= 1.609F
                dDistance /= 1.609F

                unit = "miles"
                speedUnit = "m/h"
            }

            avgSpeedView.text = "Avg speed: ${String.format("%.2f", dAvgSpeed)} $speedUnit"
            curSpeedView.text = "Cur speed: ${String.format("%.2f", dCurSpeed)} $speedUnit"
            climbView.text = "Climb: ${String.format("%.2f", dClimb)} $unit"
            calorieView.text = "Calorie: ${it.calories.toInt()}"
            distanceView.text = "Distance: ${String.format("%.2f", dDistance)} $unit"

            locations.add(latLng)
            drawLine(latLng)

            if (!firstRecorded) {
                println("start recording!")
                val cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 17f)
                mMap.animateCamera(cameraUpdate)

                val startMarkerOptions = MarkerOptions().position(latLng).title("Start")
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE))
                startMarker = mMap.addMarker(startMarkerOptions)
                firstRecorded = true
            }

            if (this::curMarker.isInitialized) {
                curMarker.remove()

                val curZoom = mMap.cameraPosition.zoom
                if (curZoom > 17f){
                    zoom = curZoom
                }
                val cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, zoom)
                mMap.animateCamera(cameraUpdate)
            }

            markerOptions = MarkerOptions().position(latLng).title("Now")
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED))
            curMarker = mMap.addMarker(markerOptions)
        })
    }

    private fun loadLocationHistory() {
        val locationsString = intent.getStringExtra(HistoryFragment.LOCATIONS).toString()
        val latLngStrings: List<String> = locationsString.split(";")

        for (i in 0 until latLngStrings.size - 1) {
            val latLngStr = latLngStrings[i].split(",")
            val parsedLatLng = LatLng(latLngStr[0].toDouble(), latLngStr[1].toDouble())

            if (i == 0) {
                if (!this::startMarker.isInitialized) {
                    val startMarkerOptions = MarkerOptions().position(parsedLatLng).title("Start")
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE))
                    startMarker = mMap.addMarker(startMarkerOptions)

                    val cameraUpdate = CameraUpdateFactory.newLatLngZoom(parsedLatLng, 17f)
                    mMap.animateCamera(cameraUpdate)
                }
            } else if (i == latLngStrings.size - 2) {
                if (!this::curMarker.isInitialized) {
                    val curLocation = MarkerOptions().position(parsedLatLng).title("End")
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED))
                    curMarker = mMap.addMarker(curLocation)
                }
            }
            locations.add(parsedLatLng)
        }
        drawLinesHistory()
    }

    private fun autoDetection() {
        sensorIntent = Intent(this, SensorService::class.java)
        if (!sensorStarted) {
            startService(sensorIntent)
            sensorStarted = true
        }
        bindService(sensorIntent, predictionViewModel, Context.BIND_AUTO_CREATE)
        predictionViewModel.activityType.observe(this, {
            val curActivityType = it.split(",")[0]
            typeView.text = "Type $curActivityType"
            ACTIVITYTYPE = it.split(",")[1]
        })
    }

    private fun drawLinesHistory() {
        for (i in 0 until locations.size) {
            polylineOptions.add(locations.get(i))
        }
        mMap.addPolyline(polylineOptions)
    }

    private fun drawLine(latLng: LatLng) {
        polylineOptions.add(latLng)
        polylines.add(mMap.addPolyline(polylineOptions))
    }

    private fun clearLines() {
        for (i in polylines.indices) polylines[i].remove()
        polylineOptions.points.clear()
    }

    fun saveMap(view: View) {
        val sharedPreferences = getSharedPreferences(SAVEPREF, Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.putBoolean(SAVEPREF, true)
        editor.apply()

        if (isAuto) stopSensor()
        stopTracking()
    }

    fun cancelMap(view: View) {
        if (isAuto) stopSensor()
        stopTracking()
    }

    private fun stopTracking() {
        firstRecorded = false
        zoom = 17f

        if (isBind){
            unbindService(locationViewModel)
            isBind = false
        }

        if (serviceStarted) {
            stopService(serviceIntent)
            serviceStarted = false
            finish()
        }
    }

    private fun stopSensor() {
        if (sensorStarted) {
            stopService(sensorIntent)
            sensorStarted = false
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        if (isAuto) stopSensor()
        stopTracking()
    }
}