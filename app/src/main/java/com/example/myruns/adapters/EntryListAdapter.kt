package com.example.myruns.adapters

import android.app.Activity
import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.example.myruns.R
import com.example.myruns.UnitDialog
import com.example.myruns.data.Entry
import kotlin.math.floor
import kotlin.math.roundToInt

class EntryListAdapter(var activity: Activity, var entryList: List<Entry>) : BaseAdapter() {
    private lateinit var unit: String

    override fun getItem(position: Int): Any {
        return entryList.get(position)
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return entryList.size
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view: View = View.inflate(activity, R.layout.list_item_history, null)

        val textViewID = view.findViewById(R.id.entry_details) as TextView
        textViewID.text =
            "${entryList.get(position).entryType}: ${entryList.get(position).activityType}, ${
                entryList.get(position).dataTime
            }"

        val unitPreferences =
            activity.getSharedPreferences(UnitDialog.UNITPREF, Context.MODE_PRIVATE)
        val unitID = unitPreferences?.getInt(UnitDialog.UNITPREF, 0)

        var disCon = entryList.get(position).distance.toDouble()
        if (unitID == R.id.imperial_option) {
            disCon /= 1.609F
            unit = "miles"
        } else {
            unit = "km"
        }

        var durationString = ""
        val du = entryList.get(position).duration
        val minuteVal = floor(du).toInt()
        val secondVal = ((du - minuteVal.toFloat()) * 60).roundToInt()
        if (secondVal == 0) {
            durationString = "$minuteVal mins"
        } else {
            durationString = "$minuteVal mins $secondVal secs"
        }

        val speedTimeID = view.findViewById(R.id.speedtime_details) as TextView
        speedTimeID.text =
            String.format("%.2f", disCon) + " " + unit + ", " + durationString

        return view
    }

    fun replace(newEntryList: List<Entry>) {
        entryList = newEntryList
    }
}