package com.example.myruns.adapters

import android.app.Activity
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.example.myruns.R


class DisplayArrayAdapter(
    var activity: Activity,
    var labelList: Array<String>,
    var entryValues: Array<String?>
) : BaseAdapter() {
    override fun getCount(): Int {
        return labelList.size
    }

    override fun getItem(p0: Int): Any {
        return labelList[p0]
    }

    override fun getItemId(p0: Int): Long {
        return p0.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view: View = View.inflate(activity, R.layout.list_item_display, null)

        val fieldText: TextView = view.findViewById(R.id.field_text)
        val fieldEdit: TextView = view.findViewById(R.id.field_edit)

        fieldText.text = labelList[position]
        fieldEdit.text = entryValues[position]
        return view
    }
}